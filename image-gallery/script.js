function minimize(object){
    var index = jQuery('.paged_post_gallery_item_minimize_btn').index(object);
    if (jQuery('.paged_post_gallery_item_minimize_btn').eq(index).hasClass('paged_post_gallery_item_minimized')){
        jQuery('.paged_post_gallery_item_minimizable').eq(index).slideDown();
        jQuery('.paged_post_gallery_item_minimize_btn').eq(index).addClass('paged_post_gallery_item_maximized');
        jQuery('.paged_post_gallery_item_minimize_btn').eq(index).removeClass('paged_post_gallery_item_minimized');
    }
    else{
        jQuery('.paged_post_gallery_item_minimizable').eq(index).slideUp();
        jQuery('.paged_post_gallery_item_minimize_btn').eq(index).addClass('paged_post_gallery_item_minimized');
        jQuery('.paged_post_gallery_item_minimize_btn').eq(index).removeClass('paged_post_gallery_item_maximized');
    }
}

function dummyRemove(object){
    var index = jQuery('.paged_post_gallery_item_delete_btn').index(object);
    jQuery('.paged_post_gallery_item').eq(index).remove();
}

jQuery(document).ready(function(){
	var new_template,object_id,object_name,object,timestamp, added_items, all_items ;
    added_items = new Array();
	jQuery('#gallery_item_add_btn').click(function(event){
        event.preventDefault();
        new_template = '<div class = "paged_post_gallery_item">' +
        '<div class = "paged_post_gallery_item_toolbar">' +
        '<div class = "paged_post_gallery_item_minimize_btn" onclick = "minimize(jQuery(this))"></div>' +
        '<div class = "paged_post_gallery_item_delete_btn" onclick = "dummyRemove(jQuery(this))"></div>' +
        '</div>';
        new_template+= '<div id="gallery_item_title" class="postbox ">' +
            '<div class="handlediv" title="Click to toggle"><br></div>' +
            '<h3 class="hndle ui-sortable-handle"><span>Gallery Item Title</span></h3>' +
            '<div class="inside"> ' +
                '<input id="gallery_item_title_" type="text" class="widefat" plaecholder="Type Gallery Item title" name="gallery_item_title_">' +
            '</div> ' +
        '</div>';
        new_template+=
            '<div class = "paged_post_gallery_item_minimizable"><div id="gallery_image" class="postbox ">' +
            '<div class="handlediv" title="Click to toggle"><br></div>' +
            '<h3 class="hndle ui-sortable-handle"><span>Add Image</span></h3>' +
            '<div class="inside"> <input class="gallery_image_upload_button" type="button" value="Upload Image">' +
            '<img alt="" id="my_gallery_image_" src="" class="widefat gallery_item_image"> ' +
            '<input type="hidden" class="widefat" id="my_gallery_image_src_" name="my_gallery_image_src_"> ' +
        '</div> ' +
        '</div>';
        new_template+='<div id="gallery_image_description" class="postbox "> ' +
        '<div class="handlediv" title="Click to toggle"><br></div><h3 class="hndle ui-sortable-handle"><span>Image Description</span></h3> ' +
        '<div class="inside">' +
        '<textarea rows="5" cols="20" class="widefat" name="gallery_image_description_" id = "gallery_image_description_"></textarea> </div> </div></div></div>';
        jQuery('#gallery_item_add').before(new_template);
        all_items = jQuery(event.currentTarget.parentNode).parent().parent().find('.widefat');
        for (var index = all_items.length-1;index>(all_items.length-5);index--){
            added_items.push(all_items[index]);
        }
        timestamp = new Date().getTime();
        for (var i =0;i<added_items.length;i++){
            object = added_items[i];
            object_id = object.getAttribute('id');
            object.setAttribute("id", object_id + timestamp);
            if (object.tagName != 'IMG') {
                object_name = object.getAttribute('name');
                object.setAttribute("name", object_name + timestamp);
            }
        }
	});

    var inserted_img_id = '';
    var inserted_img_value = '';

    jQuery('#normal-sortables').on("click", '.gallery_image_upload_button', function(event) {
        inserted_img_id = '#' + jQuery(event.target).next().attr('id');
        inserted_img_value = '#' + jQuery(event.target).next().next().attr('id');
        formfield = jQuery('#gallery_image').attr('name');
        tb_show( '', 'media-upload.php?type=image&amp;TB_iframe=true' );
        return false;
    });

    jQuery('.handlediv').off("click");

    jQuery('#normal-sortables').on("click", '.handlediv', function(event) {
        console.log(jQuery(event.target).parent().hasClass('closed'));
        if (jQuery(event.target).parent().hasClass('closed')){
            jQuery(event.target).parent().removeClass('closed');
        }
        else{
            jQuery(event.target).parent().addClass('closed');
        }

    });

    window.send_to_editor = function(html) {
        imgurl = jQuery('img',html).attr('src');
        jQuery(inserted_img_id).attr("src", imgurl);
        jQuery(inserted_img_value).val(imgurl);
        tb_remove();
    }
});