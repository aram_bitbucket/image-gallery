<?php
/*
 * Plugin Name: Image Posts with pages
 * Version: 1.0
 * Author: Hrayr Hovakimyan
 * Description: This plugin allows you to create image posts that are paged
 */
register_activation_hook( __FILE__, 'my_rewrite_flush' );

//register custom post type
function register_gallery_postType(){
    $args = array(
        'public'=>true,
        'label'=>'Gallery',
        'delete_with_user'=>true,
        'has_archive' => true,
        'publicly_queryable'=>true,
        'exclude_from_search'=>false,
        'query_var'=>true,
        'rewrite'=>array('slug' => 'gallery'),
        'supports'=>array('title','thumbnail'),
        'taxonomies'=>array('category','post_tag'),
        'labels'=>array(
            'singular_name' => __( 'Gallery', 'gallery' ),
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Gallery Item',
            'new_item'           => 'New Gallery Item',
            'edit_item'          => 'Edit Gallery Item',
            'view_item'          => 'View Gallery Item',
            'all_items'          => 'All Gallery Items',
            'search_items'       => 'Search Gallery Items',
            'not_found'          => 'No gallery items found.',
            'not_found_in_trash' => 'No gallery items found in Trash.'
        )
    );
    register_post_type('gallery', $args);
}

//solve permalink problem
function my_rewrite_flush() {
    register_gallery_postType();
    flush_rewrite_rules();
}

function add_gallery_meta_boxes($post){
    $metas = get_post_meta($post->ID);
    $meta_array_keys = array_keys($metas);
    $template = '';
    foreach ($meta_array_keys as $meta_key){
      if (strpos($meta_key, 'gallery_item_title_') !== false) {
           $meta_value = get_post_meta($post->ID,$meta_key,true);
           $template.= '<div class = "paged_post_gallery_item"><div class = "paged_post_gallery_item_toolbar">';
           $template.= '<div class = "paged_post_gallery_item_minimize_btn" onclick = "minimize(jQuery(this))"></div>';
           $template.= '<div class = "paged_post_gallery_item_delete_btn" onclick = "dummyRemove(jQuery(this))"></div></div>';
           $template.= '<div id="gallery_item_title" class="postbox "><div class="handlediv" title="Click to toggle"><br></div>';
           $template.= '<h3 class="hndle ui-sortable-handle"><span>Gallery Item Title</span></h3><div class="inside">';
           $template.= '<input id="'.$meta_key.'" type="text" class="widefat" plaecholder="Type Gallery Item title" name="'.$meta_key.'" value = "'.$meta_value.'">';
           $template.= '</div></div>';
      }
      else if (strpos($meta_key, 'my_gallery_image_src_') !== false){
          $meta_value = get_post_meta($post->ID,$meta_key,true);
          $timestamp = str_replace('my_gallery_image_src_','',$meta_key);
          $template.= '<div class = "paged_post_gallery_item_minimizable"><div id="gallery_image" class="postbox "><div class="handlediv" title="Click to toggle"><br></div>';
          $template.= '<h3 class="hndle ui-sortable-handle"><span>Add Image</span></h3><div class="inside">';
          $template.= '<input class="gallery_image_upload_button" type="button" value="Upload Image">';
          $template.= '<img alt="" id="my_gallery_image_'.$timestamp.'" src="'.$meta_value.'" class="widefat gallery_item_image">';
          $template.= '<input id="'.$meta_key.'" type="hidden" class="widefat" name="'.$meta_key.'" value = "'.$meta_value.'">';
          $template.= '</div></div>';
          //gallery_image_form($post, $custom_meta);
      }
      else if (strpos($meta_key, 'gallery_image_description_') !== false){
          $meta_value = get_post_meta($post->ID,$meta_key,true);
          $template.= '<div id="gallery_image_description" class="postbox "><div class="handlediv" title="Click to toggle"><br></div>';
          $template.= '<h3 class="hndle ui-sortable-handle"><span>Image Description</span></h3><div class="inside">';
          $template.= '<textarea rows="5" cols="20" class="widefat" name="'.$meta_key.'" id = "'.$meta_key.'">'.$meta_value.'</textarea>';
          $template.= '</div></div></div></div>';
      }
    }
    echo '<script>window.onload = function(){jQuery("#gallery_item_add").before(\''.$template.'\');}</script>';
}

function gallery_admin_init(){
    add_meta_box('gallery_item_title1', 'Gallery Item Title', 'gallery_item_title_form', 'gallery', 'normal', 'low');
    add_meta_box('gallery_image', 'Add Image', 'gallery_image_form', 'gallery', 'normal', 'low');
    add_meta_box('gallery_image_description1', 'Image Description', 'gallery_image_description_form', 'gallery', 'normal', 'low');
    add_meta_box('gallery_item_add', 'Add Gallery Item', 'gallery_image_add_button', 'gallery', 'normal', 'low');
}

function gallery_item_title_form($post){
    $custom = get_post_custom($post->ID);
    ?>
    <input type="text" class = "widefat" value = "<?php if (isset($custom['gallery_item_title1'][0])) echo $custom['gallery_item_title1'][0]; ?>" id = "gallery_item_title1" name = "gallery_item_title1"/>
<?php
}

function gallery_image_form($post){
    $custom = get_post_custom($post->ID);
    ?>
    <input class = "gallery_image_upload_button" type="button" value="Upload Image"/>
    <img width = "300" height = "225" alt="" id = "my_gallery_image" src ="<?php if (isset($custom['my_gallery_image_src'][0])) echo $custom['my_gallery_image_src'][0]; ?>" class = "widefat gallery_item_image"/>
    <input type="hidden" value = "<?php if (isset($custom['my_gallery_image_src'][0])) echo $custom['my_gallery_image_src'][0]; ?>" id = "my_gallery_image_src" name = "my_gallery_image_src" class = "widefat"/>
<?php
}

function gallery_image_description_form($post, $meta){
    $custom = get_post_custom($post->ID);
    ?>
    <textarea rows = "5" cols = "20" class = "widefat" name = "gallery_image_description1" id = "gallery_image_description1"><?php if (isset($custom['gallery_image_description1'][0])) echo $custom['gallery_image_description1'][0]; ?></textarea>
<?php
}

function gallery_image_add_button(){
    ?>
    <button id="gallery_item_add_btn">Add Gallery Item</button>
<?php
}

//load scripts for uploading image
function gallery_admin_scripts() {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
}

//load styles for gallery admin styles
function gallery_admin_styles() {
    wp_enqueue_style('thickbox');
}

//saving custom post data
function save_gallery_date($post_id){
    global $wpdb;
    $result = $wpdb->get_row('SELECT post_type FROM wp_posts WHERE id = '.$post_id);
    if ($result->post_type == 'gallery'){
        if (isset($_POST) && !empty($_POST)){
            global $wpdb;
            $post_array_keys = array_keys($_POST);
            $post_content = '';
            foreach ($post_array_keys as $post_key) {
                if ((strpos($post_key, 'gallery_item_title') !== false) or (strpos($post_key, 'my_gallery_image_src') !== false) or (strpos($post_key, 'gallery_image_description') !== false)) {
                    update_post_meta($post_id, $post_key, $_POST[$post_key]);
                }

                if ((strpos($post_key, 'gallery_item_title') !== false)){
                    $post_content.= '<h2 class = "gallery_item_title">'.$_POST[$post_key].'</h2>';
                }
                else if ((strpos($post_key, 'my_gallery_image_src') !== false)){
                    $post_content.= '<img src="'.$_POST[$post_key].'">';
                }
                else if (strpos($post_key,'gallery_image_description') !== false){
                    $post_content.= '<div class = "gallery_image_description">'.$_POST[$post_key].'</div>';
                    $post_content.= '<!--nextpage-->';
                }
            }
            $post_content = substr($post_content, 0, strlen($post_content)-15);
            $wpdb->update($wpdb->posts, array( 'post_content' => $post_content ), array( 'ID' => $post_id ));

        }
    }
    return $post_id;
}

function load_custom_gallery_style(){
    wp_register_style('pagedPostStyle', plugin_dir_url(__FILE__) . '/style.css', false, '1.0.0');
    wp_enqueue_style('pagedPostStyle');
    wp_enqueue_script('pagedPostScript', plugin_dir_url(__FILE__) . '/script.js', array(), '1.0.0', true);
}

function check_post_validity($maybe_empty){
    $post_array_keys = array_keys($_POST);
    $is_post_empty = false;
    foreach ($post_array_keys as $post_key){
        if (strpos($post_key, 'my_gallery_image_src') !== false){
            if (empty($_POST[$post_key])){
                $is_post_empty = true;
                break;
            }
        }
    }
    return $is_post_empty;
}

function add_frontend_resources(){
    wp_enqueue_style('image-gallery-style', plugin_dir_url(__FILE__) . '/image-gallery.css', false, '1.0.0');
}

function change_post_content($post_content){
    global $post;
    if ($post->post_type == 'gallery'){
        wp_link_pages( array(
            'before'      => '<div class="img-gal-top-page-links img-gal-page-links">',
            'after'       => '</div>',
            'link_before' => '<span>',
            'link_after'  => '</span>',
            'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
            'separator'   => '<span class="screen-reader-text">, </span>',
            'next_or_number'=>'next',
            'nextpagelink'=>'Next',
            'previouspagelink'=>'Previous'
        ) );

        echo '<script>
        jQuery(window).load(function() {
            jQuery(".gallery_image_description").append(jQuery(".img-gal-top-page-links").clone())
        })
        </script>';
        echo '<style>.page-links{display: none;}</style>';
    }
    return $post_content;
}




//hook part
add_filter('wp_head', 'add_frontend_resources');
add_action('admin_enqueue_scripts', 'load_custom_gallery_style');
add_action('the_content', 'change_post_content');
add_action('save_post', 'save_gallery_date');
if (isset($_GET['post_type']) && $_GET['post_type'] == 'gallery') {
    add_action('admin_print_scripts', 'gallery_admin_scripts');
    add_action('admin_print_styles', 'gallery_admin_styles');

}

add_action('init', 'register_gallery_postType');
add_action('admin_init', 'gallery_admin_init');
add_action("add_meta_boxes_gallery", "add_gallery_meta_boxes");
add_action('wp_insert_post_empty_content', 'check_post_validity');